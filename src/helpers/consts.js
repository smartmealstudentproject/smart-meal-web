export const basicUrl = 'http://localhost:56829/api';
export const staticImages = 'http://localhost:56829/static/images/';
export const rawUrl = 'http://localhost:56829';
export const metrics = [
  { name: 'Łyżki', id: 0 },
  { name: 'Łyżeczki', id: 1 },
  { name: 'Kawałki', id: 2 },
  { name: 'Sczypta', id: 3 },
  { name: 'Gram', id: 4 },
  { name: 'Kilogram', id: 5 },
  { name: 'Litr', id: 6 },
  { name: 'Mililitr', id: 7 },
];
