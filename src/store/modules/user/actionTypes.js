const rootElemet = 'Modules/user';

export const SET_CURRENT_USER = `${rootElemet}/SET_CURRENT_USER`;
export const LOGOUT_CURRENT_USER = `${rootElemet}/LOGOUT_CURRENT_USER`;
