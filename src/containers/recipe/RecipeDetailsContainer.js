import React from 'react';
import { Container } from 'react-bootstrap';
import RecipeDetails from '../../components/recipe/RecipeDetails';

const RecipeDetailsContainer = () => (
  <Container>
    <RecipeDetails />
  </Container>
);

export default RecipeDetailsContainer;
